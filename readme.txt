Alex Case
Project 1: UDP FTP

<Running the Program>
Compile the client and server programs using
#javac FTPServer.java
#javac FTPClient.java
Run the server program first with 
#java FTPServer
Then run the client program with
#java FTPClient

<Specifying File>
The file that is being transferred must be in the same folder as the server program.
To specify which file you are requesting, change the string in line 20 of the client program.

<Reliability>
I ensured reliability with an acknowledgement system on the client and server programs. The first
packet that the client receives from the server has the length of the file requested in packets. The 
client program builds an indexed arraylist of packets and acknowledges each packet received.

The server program keeps track of the number of packets acknowledged and fills an array called packetIndex
with 1's for every ack packet received.