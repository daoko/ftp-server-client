import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;


public class FTPClient {
	
	public static void main(String args[]) throws Exception{
		
	String reqFile;
	DatagramSocket clientSocket = new DatagramSocket(9876);
	InetAddress targetIP = InetAddress.getByName("localhost");
	
	byte[] sendData = new byte[128];
	byte[] receiveData = new byte[128];
	ArrayList<byte[]>  packets = new ArrayList<byte[]>();
	
	reqFile = "test.txt"; //Filename you are requesting

	requestFile(sendData, targetIP, 1234, clientSocket, reqFile);
	
	DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
	
	while(true){
		clientSocket.receive(receivePacket);
		int header = ByteBuffer.wrap(Arrays.copyOfRange(receivePacket.getData(), 0, 4)).getInt();
		
		switch(header){
		case 2: //data message
			int packetIndex = ByteBuffer.wrap(Arrays.copyOfRange(receivePacket.getData(), 4, 9)).getInt();
			byte[] data = new byte[128];
			packets.ensureCapacity(10000);
			data = Arrays.copyOf(receivePacket.getData(), 128);
			packets.add(packetIndex, data);
			System.out.println("Received packet " + packetIndex);
			ackPacket(packetIndex, targetIP, 1234, clientSocket);
			break;
			
		case 4: //fnf message
			System.out.println("File not found.");
			break;
			
		case 5: //eof message
			System.out.println("File transfer complete. Building file");
			buildFile(packets);
			return;
			
		}
	}
	}
	
	private static void buildFile(ArrayList<byte[]> packets) throws Exception {
		OutputStream out = new FileOutputStream("downloadedFile");
		for(byte[] e: packets){
			byte[] data = Arrays.copyOfRange(e, 8, e.length);
			out.write(data);
		}
		out.close();
		System.out.println("File written successfully");
		
	}

	private static void requestFile(byte[] data, InetAddress ip, int port, 
		DatagramSocket network, String filename) throws IOException{
		//Allocate request number and filename to byte array
		// <Size of array> <Request ID> <SubHeader> <up to 120 bytes of data>
		data = ByteBuffer.allocate(128).putInt(1).putInt(filename.length()).put(filename.getBytes()).array();
		DatagramPacket sendPacket = new DatagramPacket(data, data.length, ip, port);
		network.send(sendPacket);
		
	}
	
	//Sends a packet that acknowledges  the receipt of a data packet
	//Takes the packet number, an IP, port, and DatagramSocket to send the packet over
	private static void ackPacket(int packetNumber, InetAddress ip, int port, DatagramSocket network) throws IOException{
		byte[] ackdata = ByteBuffer.allocate(128).putInt(3).putInt(packetNumber).array();
		DatagramPacket ack = new DatagramPacket(ackdata, ackdata.length, ip, port);
		network.send(ack);
		System.out.println("Acknowledging packet " + packetNumber);
		
	}
	

}
