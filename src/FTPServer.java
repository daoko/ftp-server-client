import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;


public class FTPServer {

	public static void main(String args[]) throws Exception{
		
	String requestFile;
	DatagramSocket servSocket = new DatagramSocket(1234);
	int numPackets = 0;
	byte[] receiveData = new byte[128];
	ArrayList<DatagramPacket> packets = new ArrayList<DatagramPacket>();
	int[] packetIndex = null;
	
	
	//server loop reads packets sent to socket 5
	while(true){
		DatagramPacket receiveRequest = new DatagramPacket(receiveData, receiveData.length);
		servSocket.receive(receiveRequest);
		//System.out.println("Received Packet: " + receiveRequest.toString());
		//for(byte e:receiveRequest.getData())System.out.print(e);
		int request = stripHeader(receiveRequest);
		switch(request){
			case 1: //File Request
				int namelen = ByteBuffer.wrap(Arrays.copyOfRange(receiveRequest.getData(), 4, 8)).getInt();
				requestFile =  new String(Arrays.copyOfRange(receiveRequest.getData(), 8, namelen+8), "UTF-8"); //Read the filename from the data portion of the packet
				System.out.println("Packing " + requestFile);
				try{
				partitionFile(requestFile, packets);
				} 
				catch (Exception e){
					FNFError(servSocket);
					return;
				}
				numPackets = packets.size();
				packetIndex = new int[numPackets];
				for(DatagramPacket e:packets)
					servSocket.send(e);
				
				break;
			case 3: //Acknowledgment
				int ack = ByteBuffer.wrap(Arrays.copyOfRange(receiveRequest.getData(), 4, 8)).getInt();
				System.out.println("Received confirmation for packet: " + ack);
				if (packetIndex[ack] != 1){
					packetIndex[ack] = 1;
					--numPackets; //numPackets is now used to track how many have been acknowledged. 
				}
				//After all packets have been ack'd, send an EOF message
				if(numPackets == 0){
					System.out.println("File transfer complete.");
					EOFMessage(servSocket);
					return;
				}
						
		}
		
	}
	
		
	}
	
	//Takes a filename and breaks it into equal sized packets
	private static void partitionFile(String req, ArrayList<DatagramPacket> packets) throws Exception{
		File requestedFile = new File(req);
		Path dir = Paths.get(requestedFile.getPath());
		byte[] master = null;
		byte[] data = null;
		master =  Files.readAllBytes(dir);

		int bytelen = (int)requestedFile.length()/120;
		for(int i=0; i <= bytelen; i++){
			if(i*120 + 120 > master.length)
				data = Arrays.copyOfRange(master, i*120, master.length);
			else
				data = Arrays.copyOfRange(master, i*120, i*120+120); //Store 120 bytes of the file in an array
				
			byte[] builtdata = ByteBuffer.allocate(128).putInt(2).putInt(i).put(data).array(); 
			packets.add(new DatagramPacket(builtdata, builtdata.length, InetAddress.getByName("localhost"), 9876));
		}
		
	}
	
	
	
	private static void FNFError(DatagramSocket network) throws Exception{
		System.out.println("File not found");
		byte[] fnfdata = ByteBuffer.allocate(128).putInt(4).array();
		network.send(new DatagramPacket(fnfdata, fnfdata.length, InetAddress.getByName("localhost"), 9876));
	}
	
	
	
	private static void EOFMessage(DatagramSocket network) throws Exception{
		System.out.println("Sending end of file acknowledgment");
		byte[] eofdata = ByteBuffer.allocate(128).putInt(5).array();
		network.send(new DatagramPacket(eofdata, eofdata.length, InetAddress.getByName("localhost"), 9876));
	}
	
	
	//Reads the first  4 bytes of a packet
	//Returns the value in an int to be interpreted as a request
	//Takes a DatagramPacket
	//Returns an int
	private static int stripHeader(DatagramPacket p){
		return ByteBuffer.wrap(Arrays.copyOfRange(p.getData(), 0, 4)).getInt();
		
	}
}
